#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/boost/graph/graph_traits_Polyhedron_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <iostream>
#include <fstream>
#include <random>


#define MAX_SOMMETS 50
#define MAX_PROF 5

typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
typedef CGAL::Polyhedron_3<Kernel> Polyhedron;
typedef Polyhedron::Facet_iterator Facet_iterator;
typedef Polyhedron::Vertex_iterator Vertex_iterator;
typedef Polyhedron::Halfedge_iterator Halfedge_iterator;
typedef Polyhedron::Halfedge_around_facet_circulator Halfedge_facet_circulator;

typedef std::pair<double,double> Extremum;
typedef std::map<Vertex_iterator, int> Vertex_int_map;


struct Couleur
{
	double r;
	double g;
	double b;
	double a;
};


struct Sommet
{
    double x;
    double y;
    double z;
};

bool operator<(const Sommet & s1, const Sommet & s2)
{
    bool cmp = false;
    
    if((s1.x < s2.x) || (s1.x == s2.x && s1.y < s2.y) || (s1.x == s2.x && s1.y == s2.y && s1.z < s2.z))
        cmp = true;

    return cmp;
}

typedef std::map<Vertex_iterator, Sommet> Nouveaux_sommets_map;
typedef std::map<Sommet, int> Sommet_int_map;


struct Face
{
    int id_sommet1;
    int id_sommet2;
    int id_sommet3;
};


class Noeud
{
	private:
        int profondeur;
		Extremum x_extremum;
		Extremum y_extremum;
		Extremum z_extremum;
        double xmil;
        double ymil;
        double zmil;
		std::list<Vertex_iterator> sommets;
        std::vector<Noeud> fils;

	public:
		Noeud(int = 0, Extremum = std::make_pair(0,0), Extremum y = std::make_pair(0,0), Extremum z = std::make_pair(0,0));
        void AjoutSommet(Vertex_iterator &);
        void AjoutFils(Noeud);
        void SuppressionSommets();
        int GetProfondeur() const;
        Extremum GetXExtremum() const;
        Extremum GetYExtremum() const;
        Extremum GetZExtremum() const;
        std::list<Vertex_iterator> GetSommets() const;
        std::vector<Noeud> & GetFils();

};

// Constructeur de la classe Noeud
Noeud::Noeud(int prof, Extremum x, Extremum y, Extremum z):
	profondeur(prof), x_extremum(x), y_extremum(y), z_extremum(z)
{

}


// Méthodes de la classe Noeuds

void Noeud::AjoutSommet(Vertex_iterator & v)
{
    sommets.push_back(v);
}

void Noeud::AjoutFils(Noeud n)
{
    fils.push_back(n);
}

void Noeud::SuppressionSommets()
{
    sommets.clear();
}

int Noeud::GetProfondeur() const
{
    return profondeur;
}

Extremum Noeud::GetXExtremum() const
{
    return x_extremum;
}

Extremum Noeud::GetYExtremum() const
{
    return y_extremum;
}

Extremum Noeud::GetZExtremum() const
{
    return z_extremum;
}

std::list<Vertex_iterator> Noeud::GetSommets() const
{
    return sommets;
}

std::vector<Noeud> & Noeud::GetFils()
{
    return fils;
}


// Ajout de 8 fils à un noeud de l'octree
void AjoutFils(Polyhedron & mesh, Noeud & noeud)
{
    // obtention des extremum de la boite englobante associée au noeud
    double xmin = noeud.GetXExtremum().first;
    double xmax = noeud.GetXExtremum().second;
    double xmil = (xmax+xmin)/2;

    double ymin = noeud.GetYExtremum().first;
    double ymax = noeud.GetYExtremum().second;
    double ymil = (ymax+ymin)/2;

    double zmin = noeud.GetZExtremum().first;
    double zmax = noeud.GetZExtremum().second;
    double zmil = (zmax+zmin)/2;

    int prof_fils = noeud.GetProfondeur() + 1; // profondeur des fils du noeud
    
    // Création des 8 fils
    noeud.AjoutFils(Noeud(prof_fils, std::make_pair(xmin, xmil), std::make_pair(ymin, ymil), std::make_pair(zmin, zmil)));
    noeud.AjoutFils(Noeud(prof_fils, std::make_pair(xmin, xmil), std::make_pair(ymin, ymil), std::make_pair(zmil, zmax)));
    noeud.AjoutFils(Noeud(prof_fils, std::make_pair(xmin, xmil), std::make_pair(ymil, ymax), std::make_pair(zmin, zmil)));
    noeud.AjoutFils(Noeud(prof_fils, std::make_pair(xmin, xmil), std::make_pair(ymil, ymax), std::make_pair(zmil, zmax)));
    noeud.AjoutFils(Noeud(prof_fils, std::make_pair(xmil, xmax), std::make_pair(ymin, ymil), std::make_pair(zmin, zmil)));
    noeud.AjoutFils(Noeud(prof_fils, std::make_pair(xmil, xmax), std::make_pair(ymin, ymil), std::make_pair(zmil, zmax)));
    noeud.AjoutFils(Noeud(prof_fils, std::make_pair(xmil, xmax), std::make_pair(ymil, ymax), std::make_pair(zmin, zmil)));
    noeud.AjoutFils(Noeud(prof_fils, std::make_pair(xmil, xmax), std::make_pair(ymil, ymax), std::make_pair(zmil, zmax)));


    // Ajout chaque sommets dans un des 8 fils
    std::list<Vertex_iterator> sommets = noeud.GetSommets();
    std::vector<Noeud> & list_fils = noeud.GetFils();

    int x_pos;
    int y_pos;
    int z_pos;
    int pos; // position du sommet dans l'un des 8 fils du noeud

    for(auto s = sommets.begin(); s != sommets.end(); s++)
    {
        auto point = (*s)->point();

        x_pos = (point.x() < xmil) ? 0 : 1 ;

        y_pos = (point.y() < ymil) ? 0 : 1 ;

        z_pos = (point.z() < zmil) ? 0 : 1 ;

        pos = z_pos+2*(y_pos+2*x_pos);

        list_fils[pos].AjoutSommet(*s);
    }


    // si la profondeur maximale n'est pas atteinte, crée des fils pour chaque fils qui possède plus que le maximum de sommets (50 par défaut)

    if(prof_fils < MAX_PROF)
    {
        Noeud fils;

        for(int i=0; i<8; i++)
        {
            fils = noeud.GetFils()[i];

            if(fils.GetSommets().size() > MAX_SOMMETS)
                AjoutFils(mesh, noeud.GetFils()[i]);
        }
    }
    

    // suppression de la liste des sommets qui ont été répartis dans les 8 fils
    noeud.SuppressionSommets();
}


//Création d'un arbre octree (8 fils pour chaque noeuds non feuille)
Noeud octree(Polyhedron & mesh)
{
	double x,y,z;

	// extremum de la boite englobante associée au maillage
	double xmin = mesh.vertices_begin()->point().x();
	double xmax = xmin;
	double ymin = mesh.vertices_begin()->point().y();
	double ymax = ymin;
	double zmin = mesh.vertices_begin()->point().z();
	double zmax = zmin;

	// parcours tous les sommets pour obtenir les extremum de la boite englobante associée au maillage
	for(Vertex_iterator i = mesh.vertices_begin(); i != mesh.vertices_end(); ++i) 
	{
		x = i->point().x();
		y = i->point().y();
		z = i->point().z();

		if(x < xmin)
			xmin = x;
		else if(x > xmax)
			xmax = x;

		if(y < ymin)
			ymin = y;
		else if(y > ymax)
			ymax = y;

		if(z < zmin)
			zmin = z;
		else if(z > zmax)
			zmax = z;
	}

    // création de la racine
	Noeud racine(0, std::make_pair(xmin, xmax), std::make_pair(ymin, ymax), std::make_pair(zmin, zmax));

    // ajout de tous les sommets du maillage
    for(Vertex_iterator i = mesh.vertices_begin(); i != mesh.vertices_end(); ++i) 
        racine.AjoutSommet(i);

	AjoutFils(mesh, racine); // création des 8 fils de la racine

    return racine; // renvoie la racine de l'arbre qui permet d'accéder à tous les noeuds de l'octree
}


/*
 La fonction associe un identifiant à chaque sommet, chaque sommets d'une même feuille possède le même identifiant
 La fonction va aussi permettre d'obtenir un unique sommet par feuille en calculant la moyenne des sommets de chaque feuille
*/
void VisiteurNoeuds(Noeud & noeud, Vertex_int_map & feuille, int & id, Nouveaux_sommets_map & nouveaux_sommets_map, Sommet_int_map & id_sommets)
{
    std::vector<Noeud> fils = noeud.GetFils(); // récupère le vecteur contenant les fils du noeud

    // moyenne des sommets appartenant à une feuille
    double moy_x;
    double moy_y;
    double moy_z;

    // si le vecteur est vide, le noeud est une feuille, on associe donc un identifiant identique à chacun de ses sommets
    if(fils.size() == 0)
    {
        id++; // incrémentation de l'identifiant pour une nouvelle feuille

        // initialisation de la moyenne des sommet de la feuille
        moy_x = 0;
        moy_y = 0;
        moy_z = 0;

        std::list<Vertex_iterator> sommets = noeud.GetSommets(); // liste des sommets de la feuille

        for(auto s = sommets.begin(); s != sommets.end(); s++)
        {
            feuille[*s] = id;
            moy_x += (*s)->point().x() / sommets.size();
            moy_y += (*s)->point().y() / sommets.size();
            moy_z += (*s)->point().z() / sommets.size();
        }

        // création d'un sommet à partir de la moyenne des sommets de la feuille
        for(auto s = sommets.begin(); s != sommets.end(); s++)
        {
            Sommet nouveau_sommet;

            nouveau_sommet.x = moy_x;
            nouveau_sommet.y = moy_y;
            nouveau_sommet.z = moy_z;

            nouveaux_sommets_map[*s] = nouveau_sommet; // association d'un sommet avec le sommet simplifié de la feuille
            id_sommets[nouveau_sommet] = -1; // on initialise un identifiant pour le sommet, il servira lors de l'écriture du nouveau fichier OFF
        }
        
    }
    else // sinon, le noeud possède 8 fils et on appelle la fonction visiteur sur chacun de ses fils
    {
        for(int i=0; i<8; i++)
            VisiteurNoeuds(fils[i], feuille, id, nouveaux_sommets_map, id_sommets);
    }
}

/*
 sauvegarde du maillage coloré à partir d'un octree
 une partie de la fonction c'est inspirée d'un exemple de CGAL : https://doc.cgal.org/latest/Polyhedron/Polyhedron_2polyhedron_prog_off_8cpp-example.html
*/
void SauveColorFileOff(std::string & filename, Polyhedron & mesh, Vertex_int_map & feuille)
{
	std::map<int, Couleur> couleurs; // association d'une couleur à chaque feuille de l'octree défini par un identifiant entier
	int id;

	// générer un nombre aléatoire entre 0 et 1
	std::random_device rd;
    std::default_random_engine eng(rd());
    std::uniform_real_distribution<double> distr(0, 1);

	std::ofstream file(filename+".off");

	// calcul du nombre d'arêtes
	unsigned int nbHalfEdges = 0;
	for (Halfedge_iterator i = mesh.halfedges_begin(); i != mesh.halfedges_end(); ++i) 
		++nbHalfEdges;

	unsigned int nbEdges = nbHalfEdges / 2; // pour le nombre d'arêtes, on divise le nombre de demi arêtes obtenu par deux

	// correspond à la première ligne du fichier OFF avec la deuxième ligne et le nombre de sommets, de faces et d'arêtes
	file << "COFF" << std::endl << mesh.size_of_vertices() << " " << mesh.size_of_facets() << " " << nbEdges << std::endl; // mets le nb de sommets, faces et arêtes dans le fichier OFF

	// on met tous les points dans le fichier OFF avec leur couleur associée.
	int iPoint = 0;
	for (Vertex_iterator i = mesh.vertices_begin(); i != mesh.vertices_end(); ++i) 
	{
		file << i->point() << "    "; // on écrit les coordonnées de chaque sommet

        id = feuille[i]; // identifiant de la feuille dont appartient le sommet

		if (couleurs.find(id) == couleurs.end()) // si aucune couleur n'a été défini pour cet identifiant
		{
			// créé une couleur aléatoire
			Couleur couleur;
			couleur.r = distr(eng); // couleur rouge entre 0 et 1 
			couleur.g = distr(eng); // couleur verte entre 0 et 1 
			couleur.b = distr(eng); // couleur bleue entre 0 et 1 
			couleur.a = 0.75; // transparence
			couleurs[id] = couleur; // on assigne la couleur générée à l'identifiant
		}

		// on met la couleur dans le fichier OFF et on va à la ligne à chaque nouveau sommet
		file << couleurs[id].r << " " << couleurs[id].g << " " << couleurs[id].b << " " << couleurs[id].a << std::endl;
	}

	/* On met ensuite pour chaque face une ligne comprenant :
			- le nombre de sommets/arêtes de la face
			- quel sommets correspondent à la face */

	for(Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i)
	{
		Halfedge_facet_circulator j = i->facet_begin(); // on met le circulateur des demi-arêtes à la première arête.

        CGAL_assertion( CGAL::circulator_size(j) >= 3); 

		file << CGAL::circulator_size(j) << " "; // écrit le nombre d'arête à chaque début de ligne

		do  // on écrit les sommets de la face
		{
            file << " " << std::distance(mesh.vertices_begin(), j->vertex()); // on ecrit les sommets correspondants à la face
        } while ( ++j != i->facet_begin());

        file << std::endl;
	}
}


// Crée un nouveau maillage avec une simplification des sommets et des faces
void SimplificationMaillage(Polyhedron & mesh, Vertex_int_map & feuille, Nouveaux_sommets_map & nouveaux_sommets_map, Sommet_int_map & id_sommets, std::vector<Sommet> & nouveaux_sommets, std::vector<Face> & nouvelles_faces)
{
    int id_sommet = 0;

    // parcours de chaque sommet du maillage initial afin de récupérer les nouveaux sommets du maillage simplifié et leur associer un identifiant
    for(Vertex_iterator v = mesh.vertices_begin(); v != mesh.vertices_end(); ++v) 
	{
        Sommet nouveau_sommet = nouveaux_sommets_map[v]; // nouveau sommet obtenu par la moyenne des sommets de la feuille de l'octree

        // test si un identifiant n'a pas encore été associé
		if(id_sommets[nouveau_sommet] == -1)
        {
            // ajout du nouveau sommet dans la liste
            nouveaux_sommets.push_back(nouveau_sommet);

            // attribution d'un identifiant au nouveau sommet
            id_sommets[nouveau_sommet] = id_sommet;

            id_sommet++; // incrémentation de l'identifiant pour le nouveau sommet
        }
	}

    /*
     parcours de chaque face du maillage initial afin de vérifier si ses trois sommets appartiennent à des feuilles différentes de l'octree
     si c'est le cas, on crée une nouvelle face qui contiendra les identifiants des nouveaux sommets issue de la simplification du maillage
    */
    int feuille1, feuille2, feuille3;
    Vertex_iterator sommet1, sommet2, sommet3;
    Sommet nouveau_sommet1, nouveau_sommet2, nouveau_sommet3;

    for(Facet_iterator f = mesh.facets_begin(); f != mesh.facets_end(); ++f)
	{
		Halfedge_facet_circulator h = f->facet_begin(); // on met le circulateur des demi-arêtes à la première arête.

        // test si la face possède 3 sommets
		if(CGAL::circulator_size(h) == 3)
        {
            sommet1 = h->vertex();
            feuille1 = feuille[sommet1]; // identifiant de la feuille dans laquelle se trouve le premier sommet de la face
            ++h;

            sommet2 = h->vertex();
            feuille2 = feuille[sommet2]; // identifiant de la feuille dans laquelle se trouve le deuxième sommet de la face
            ++h;

            sommet3 = h->vertex();
            feuille3 = feuille[sommet3]; // identifiant de la feuille dans laquelle se trouve le troisième sommet de la face
            ++h;

            // test si les trois sommets appartiennent à trois feuille différentess
            if(feuille1 != feuille2 && feuille2 != feuille3 && feuille1!= feuille3)
            {
                // création d'une nouvelle face composé des nouveaux sommets
                Face nouvelle_face;

                nouveau_sommet1 = nouveaux_sommets_map[sommet1]; // récupération du nouveau sommet du maillage simplifié
                nouvelle_face.id_sommet1 = id_sommets[nouveau_sommet1]; // récupération de l'identifiant du nouveau sommet

                nouveau_sommet2 = nouveaux_sommets_map[sommet2]; 
                nouvelle_face.id_sommet2 = id_sommets[nouveau_sommet2]; 

                nouveau_sommet3 = nouveaux_sommets_map[sommet3]; 
                nouvelle_face.id_sommet3 = id_sommets[nouveau_sommet3];

                nouvelles_faces.push_back(nouvelle_face); // ajout d'une nouvelle face dans la liste
            }
        }
	}
}


// sauvegarde du maillage coloré à partir d'un octree
void SauveSimplifyFileOff(std::string & filename, std::vector<Sommet> & nouveaux_sommets, std::vector<Face> & nouvelles_faces)
{
    std::ofstream file(filename+".off");

    // correspond à la première ligne du fichier OFF avec la deuxième ligne et le nombre de sommets et de faces
	file << "OFF" << std::endl << nouveaux_sommets.size() << " " << nouvelles_faces.size() << " " << 0 << std::endl; // mets le nb de sommets et de faces dans le fichier OFF

	// on ajoute la liste des sommets dans le fichier OFF
    for(auto s = nouveaux_sommets.begin(); s != nouveaux_sommets.end(); s++)
        file << s->x << " " << s->y << " " << s->z << std::endl;

    // on ajoute la liste des faces avec l'identifiant de leurs sommets dans le fichier OFF
	for(auto f = nouvelles_faces.begin(); f != nouvelles_faces.end(); f++)
        file << 3 << "  " << f->id_sommet1 << " " << f->id_sommet2 << " " << f->id_sommet3 << std::endl;
}



int main(int argc, char* argv[])
{
	if (argc < 3) 
	{
		std::cerr << "Il manque un parametre au programme. Veuillez lui donner en entree un nom de fichier au format off et un nom de fichier de sortie" << std::endl;
		return 1;
	}
	
	Polyhedron mesh;
	std::ifstream input(argv[1]);

	if (!input || !(input >> mesh) || mesh.is_empty())
	{
		std::cerr << "Le fichier donne n'est pas un fichier off valide." << std::endl;
		return 1;
	}


    if(argc > 2) 
    { 
		std::string color_file_name = argv[2];

        Noeud racine_octree = octree(mesh); // crée un octree et renvoie sa racine

        Vertex_int_map feuille; // variable qui associe un identifiant à chaque sommet du maillage
        int id = -1; // identifiant qui sera associé à chaque sommet contenu dans les feuilles

        Nouveaux_sommets_map nouveaux_sommets_map; // association des sommets du maillages initial avec les sommets du nouveau maillage
        Sommet_int_map id_sommets; // association d'un identifiant pour chaque sommet du nouveau maillage

        VisiteurNoeuds(racine_octree, feuille, id, nouveaux_sommets_map, id_sommets); // fonction qui visite chaque noeud et associe un identifiant à chaque sommet des feuilles, chaque sommet d'une même feuille possède le même identifiant

		SauveColorFileOff(color_file_name, mesh, feuille); // sauvegarde du maillage coloré au format OFF


        if(argc > 3)
        {
            std::string simplify_file_name = argv[3];

            std::vector<Sommet> nouveaux_sommets;
            std::vector<Face> nouvelles_faces;

            SimplificationMaillage(mesh, feuille, nouveaux_sommets_map, id_sommets, nouveaux_sommets, nouvelles_faces); // crée un nouveau maillage avec une simplification des sommets et des faces
            SauveSimplifyFileOff(simplify_file_name, nouveaux_sommets, nouvelles_faces); // sauvegarde du simplifié au format OFF
        }
    }

  
	return 0;
}