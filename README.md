# Remaillage_Prudent_Szabo


## Présentation du projet

Ce programme a pour objectif de réaliser une simplification de maillage en diminuant le nombre de sommets et de faces utilisés.  

Il utilise pour cela une structure de données de type octree, un arbre dans lequel chaque nœud non feuille possède exactement huit fils.  
L'octree va permettre de répartir les sommets du maillage en plusieurs groupes selon leur position dans le maillage.

Le programme utilise en entrée un maillage représenté par un fichier OFF. Des exemples sont disponibles dans le dossier data du répertoire du projet.  
Deux fichiers OFF peuvent être obtenus en sortie du programme selon le nombre de paramètres fournis. Le premier représente le maillage fourni en entrée, coloré à partir de la répartition réalisée avec l'octree. Le second représente la simplification du maillage initial réalisé grâce à l'octree.




## Compilation

Créer un dossier de compilation (build par exemple) dans le répertoire du projet et utiliser la commande cmake et make.
```
mkdir build
cd build
cmake ../src/
make
```


## Utilisation

Un fichier exécutable nommé octree est obtenu après l'étape de compilation.
Il permet d'obtenir à partir d'un maillage en entrée, une colorisation de se maillage (obtenue à partir d'un octree) et une simplification du maillage avec un nombre de sommets et de faces réduit.

Le fichier exécutable prend jusqu'à 3 paramètres en entrée :
- Le premier obligatoire est un fichier au format .off représentant le maillage d'origine
- Le deuxième optionnel est le nom du fichier de sortie représentant une colorisation du maillage à partir d'un octree
- Le troisième optionnel est le nom du fichier de sortie représentant une simplification du maillage

Exemple avec le maillage horse.off disponible dans le dossier data du projet.
```
cd build
./octree ../data/horse.off horse_octree horse_simplify
```


## Visualisation

Il est possible de visualiser les maillages représentés par des fichiers OFF à l'aide du logiciel libre [MeshLab](https://www.meshlab.net/).



